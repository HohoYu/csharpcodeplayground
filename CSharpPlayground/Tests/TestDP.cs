﻿using System;
using System.Collections.Generic;
using System.Text;

using CSharpPlayground.Modules;
using CSharpPlayground.Tests;

namespace CSharpPlayground.Tests
{
    enum printdirType { None, printdir }
    public class TestDP
    {
        static printdirType c = printdirType.None;
        public static void Test()
        {
            c = TestCase.GetTestCase<printdirType>(7);
            switch (c)
            {
                case printdirType.printdir:
                    Dirprinter dp = new Dirprinter();
                    dp.Printdir($@"./filename.txt", $@"\\forida.local.hk\share\Company2\Forida-2\Technical\Database\Tools\Revit\Dynamo\Dynamo List");
                    break;

                default:
                    break;
            }
        }
    }
}
