﻿using CSharpPlayground.Tests;
using System;
using System.Collections.Generic;

namespace Codeplayground
{
    enum Testcategory { None, log, printdir, casewindow, casewindow2 }
    class Program
    {
        static Testcategory c = Testcategory.None;

        [STAThread]
        static void Main(string[] args)
        {
            c = TestCase.GetTestCase<Testcategory>(10);
            switch (c)
            {
                case Testcategory.log:
                    Testlog.Test();
                    break;

                case Testcategory.printdir:
                    TestDP.Test();
                    break;

                case Testcategory.casewindow:
                    TestCaseWindow.Test();
                    break;

                case Testcategory.casewindow2:
                    TestCaseWindow.Test2();
                    break;

                default:
                    break;
            }


            Console.ReadKey();
        }
    }
}
