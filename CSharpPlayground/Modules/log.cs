﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace CSharpPlayground.Modules
{

    public class Log4
    {

        private _Log4 log;
        public Log4(string appname, bool config)
        {
            string loc = System.Reflection.Assembly.GetCallingAssembly().Location;
            log = new _Log4(appname, loc, config);
        }
        public void SetAppname(string appname)
        {
            log._appname = appname;
        }
        public void Log(object obj)
        {
            log.Log(obj);
        }
        public void Log(Exception ex)
        {
            log.Log(ex);
        }
        public void Log<T>(List<T> list)
        {
            log.Log(list);
        }
        public void Log<T,S>(Dictionary<T,S> mapping)
        {
            log.Log(mapping);
        }
    }
    internal class _Log4
    {
        internal _Log4(string appname, string loc, bool createsubdirectory)
        {
            _loc = loc;
            _appname = appname;
            _sub = createsubdirectory;
            NewDir(_logdir);
            //NewDir(_errlogdir);
            _path = GetLatestFile();
            _errpath = GetLatestErrFile();
        }
        private string _path;
        internal string _appname;
        private string _errpath;
        internal string _loc;
        private bool _sub { get; set; }
        private string _logdir =>$@"{Path.Combine(Path.GetDirectoryName(_loc), Path.GetFileNameWithoutExtension(_loc))}_log{(_sub? "/"+_appname:"")}";
        //private string _errlogdir => $@"{Path.Combine(Path.GetDirectoryName(_loc), Path.GetFileNameWithoutExtension(_loc))}_errlog{_appname}";
        private string prefix => $@"[{_appname}-{DateTime.Now.ToString("yyyyMMddHHmmss")}]_";
        //internal void Log(string logmsg)
        //{
        //    _path = Check(_path);
        //    Newline(logmsg);
        //    Newline("");
        //}

        internal void Log(object obj)
        {
            Newline(obj, ref _path);
            Newline("", ref _path);
        }

        internal void Log(Exception ex)
        {
            Newline(ex.ToString(), ref _errpath);
            Newline("", ref _errpath);
        }

        internal void Log<T>(List<T> list)
        {
            string msg = "";
            for (int i = 0; i < list.Count; i++)
            {
                msg += newspace(i) + list[i].ToString() + "\n";
            }
            Log(msg);
        }

        internal void Log<T,S>(Dictionary<T, S> mapping)
        {
            string msg = "";
            int count = 0;
            foreach (var item in mapping)
            {
                msg += $@"{newspace(count)}{item.Key.ToString()}{newcolon(count)}{item.Value.ToString()}{"\n"}";
                count++;
            }
            Log(msg);
        }
        #region helper functions
        private string GetLatestFile()
        {
            string result = "";
            string[] files = Directory.GetFiles(_logdir, "*_log.txt");
            DateTime latest = new DateTime(0);
            foreach (string file in files)
            {
                DateTime temp = File.GetLastWriteTime(file);
                if (DateTime.Compare(temp, latest) > 0)
                {
                    latest = temp;
                    result = file;
                }
            }
            return (result == "")? Newpath("_log.txt"): result; 
        }
        private string GetLatestErrFile()
        {
            string result = "";
            string[] files = Directory.GetFiles(_logdir, "*_errlog.txt");
            DateTime latest = new DateTime(0);
            foreach (string file in files)
            {
                DateTime temp = File.GetLastWriteTime(file);
                if (DateTime.Compare(temp, latest) > 0)
                {
                    latest = temp;
                    result = file;
                }
            }
            return (result == "") ? Newpath("") : result;
        }
        private string CheckLimit(string path)
        {
            if (!File.Exists(path))
            {
                return path;
            }
            else if (Count(path) < 1000)
            {
                return path;
            }
            else
            {
                return Newpath(path);
            }
        }

        private int Count(string path)
        {
            int result = 0;
            result = File.ReadLines(path).Count();
            return result;
        }

        private void NewDir(string dir)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
        private string Newpath(string oldpath)
        {
            string logdir = _logdir;
            string path = Path.Combine(logdir, $@"{prefix}errlog.txt");
            if (oldpath.EndsWith("_log.txt"))
            {
                path = Path.Combine(logdir, $@"{prefix}log.txt");
            }
            return path;
        }

        private string Newerrpath()
        {
            //string logdir = _errlogdir;
            string logdir = _logdir;
            string path = Path.Combine(logdir, $@"{prefix}errlog.txt");
            return path;
        }

        private void Newline(object obj, ref string path)
        {
            path = CheckLimit(path);
            string msg = obj.ToString();
            using (StreamWriter sw = new StreamWriter(path, true))
            {
                if (msg != "")
                {
                    sw.WriteLine($@"{DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss")}{"\n"}{msg}");
                }
                else
                {
                    sw.WriteLine("");
                }
            }
        }

        private string newspace(int index)
        {
            string result = index.ToString();
            while (result.Length < 5)
            {
                result += ".";
            }
            return result;
        }

        private string newcolon(int index)
        {
            string result = ":  ";
            int placeholder = 7 - index.ToString().Length;
            while (result.Length < placeholder)
            {
                result = " " + result;
            }
            return result;
        } 
        #endregion
    }
}
