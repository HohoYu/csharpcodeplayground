﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace CSharpPlayground.Modules
{
    /// <summary>
    /// Interaction logic for CaseListWindow.xaml
    /// </summary>
    public partial class CaseListWindow : Window
    {
        private string cachepath => System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "cache.json");
        public Item Case { get; private set; }
        private JObject _cache;
        private string _name;
        public CaseListWindow(List<string> cases, string name)
        {
            InitializeComponent();
            _name = name;
            _cache = ReadCache(cachepath);
            string value="";
            foreach (JProperty item in _cache.Properties())
            {
                if (item.Name == name)
                {
                    value = item.Value.ToString();
                }
            }
            List<Item> items = cases.Select(x => new Item(x)).ToList();
            CaseList.ItemsSource = items;
            int i = 0;
            bool check = true;
            foreach (Item item in items)
            {
                item.IsOn = false;
                if (item.Text == value)
                {
                    item.IsOn = true;
                    Case = item;
                    check = false;
                }
                i++;
            }
            if(check)
            {
                items[0].IsOn = true;
                Case = items[0];
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton button = sender as RadioButton;
            Item item = GetDataContext(button) as Item;

            Case = item;

        }

        public class Item
        {
            public string Text { get; set; }

            public Item(string text)
            {
                Text = text;
            }

            public bool IsOn { get; set; }
        }

        #region Data Context
        private object GetDataContext(FrameworkElement e)
        {
            if (e != null && e.TemplatedParent != null)
            {
                ContentPresenter template = e.TemplatedParent as ContentPresenter;
                return template.Content;
            }

            return null;
        }

        private void SetDataContext(FrameworkElement e, object value)
        {
            if (e != null && e.TemplatedParent != null)
            {
                ContentPresenter template = e.TemplatedParent as ContentPresenter;
                template.Content = value;
            }
        }
        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
            UpdateCache();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void UpdateCache()
        {
            try
            {
                _cache.Property(_name).Value = Case.Text;
            }
            catch
            {
                _cache.Add(_name, Case.Text);
            }
            System.IO.File.WriteAllText(cachepath, _cache.ToString());
        }

        //private string ReadCache(string dir)
        //{
        //    if(!System.IO.File.Exists(dir))
        //    {
        //        return "";
        //    }
        //    else
        //    {
        //        using (System.IO.StreamReader sr = new System.IO.StreamReader(dir))
        //        {
        //            return sr.ReadLine();
        //        } 
        //    }
        //}

        private JObject ReadCache(string dir)
        {
            if (!System.IO.File.Exists(dir))
            {
                return new JObject();
            }
            else
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(dir))
                {
                    return JObject.Parse(sr.ReadToEnd());
                }
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                OKButton_Click(sender, e);
            }
        }
    }
}
