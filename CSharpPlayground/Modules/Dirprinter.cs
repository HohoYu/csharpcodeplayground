﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace CSharpPlayground.Modules
{
    internal class Dirprinter
    {
        internal void Printdir(string txtpath, string path)
        {
            if (!IsDirectoryEmpty(path))
            {
                string[] files = Directory.GetFileSystemEntries(path);
                foreach (string file in files)
                {
                    string name = Path.GetFileNameWithoutExtension(file);
                    using (StreamWriter sw = new StreamWriter(txtpath, true))
                    {
                        sw.WriteLine(name);
                    }
                }
            }
        }
        public bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }
}
