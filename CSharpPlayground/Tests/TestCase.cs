﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPlayground.Tests
{
    public class TestCase
    {
        public static T GetTestCase<T>(int n) where T : struct, IConvertible
        {
            List<T> list = new List<T>();
            for (int i = 0; i < n; i++)
            {
                try
                {
                    list.Add((T)(object)i);
                }
                catch
                {

                }
            }

            Console.WriteLine($"Please Choose a {typeof(T).Name}");
            if (list.Count > 0)
                foreach (var i in list)
                {
                    Console.WriteLine($"{list.IndexOf(i)}. {i}");
                }

            if (!Int32.TryParse(Console.ReadLine(), out int value))
                return default(T);
            else

                return (T)(object)(value);
        }
    }
}
