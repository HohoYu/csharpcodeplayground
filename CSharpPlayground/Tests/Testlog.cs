﻿using System;
using System.Collections.Generic;
using System.Text;
using CSharpPlayground.Modules;
using CSharpPlayground.Tests;

namespace CSharpPlayground.Tests
{
    enum logTestType { None, testlog, testlogobj, testlist, testlogerr, all, testdict, testnotcreatesubdir, }
    public class Testlog
    {
        static logTestType c = logTestType.None;
        public static void Test()
        {
            c = TestCase.GetTestCase<logTestType>(10);
            switch (c)
            {
                case logTestType.testlog:
                    Log4 log = new Log4("testlog", true);
                    for (int i = 0; i < 1000; i++)
                    {
                        log.Log($@"testing{i}");
                    }
                    break;

                case logTestType.testlogobj:
                    Log4 log2 = new Log4("testlogobj", true);
                    for (int i = 0; i < 1000; i++)
                    {
                        log2.Log(log2);
                    }
                    break;

                case logTestType.testlist:
                    Log4 log3 = new Log4("testlist", true);
                    List<object> list = new List<object>();
                    for (int i = 0; i < 2000; i++)
                    {
                        list.Add($@"testinglist{i}");
                    }
                    log3.Log(list);
                    break;

                case logTestType.testlogerr:
                    Log4 log4 = new Log4("testlogerr", true);
                    //log4.Log(new EntryPointNotFoundException());
                    //log4.Log(new DivideByZeroException());
                    //log4.Log(new FormatException());
                    dummymethod1(log4);
                    break;

                case logTestType.all:
                    Log4 log5 = new Log4("all", true);
                    for (int i = 0; i < 100; i++)
                    {
                        log5.Log($@"testing{i}");
                        log5.Log(log5);
                        List<object> list1 = new List<object>();
                        for (int j = 0; j < 20; j++)
                        {
                            list1.Add($@"testinglist{j}");
                        }
                        log5.Log(list1);
                        log5.Log(new EntryPointNotFoundException());
                        log5.Log(new DivideByZeroException());
                        log5.Log(new FormatException());
                    }
                    break;

                case logTestType.testdict:
                    Log4 log6 = new Log4("testdict", true);
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    for (int i = 0; i < 2000; i++)
                    {
                        dict.Add("testkey" + i, "testvalue" + i);
                    }
                    log6.Log(dict);
                    break;
                default:
                    break;

                case logTestType.testnotcreatesubdir:
                    Log4 log7 = new Log4("testnosub", false);
                    for (int i = 0; i < 1000; i++)
                    {
                        log7.Log($@"testing no sub {i}");
                        log7.Log(new EntryPointNotFoundException());
                    }
                    break;
            }
        }
        #region dummymethods
        public static void dummymethod1(Log4 log4)
        {
            try
            {
                dummymethod2();
            }
            catch (Exception ex)
            {
                log4.Log(ex);
            }

        }
        public static void dummymethod2()
        {
            dummymethod3();
        }
        public static void dummymethod3()
        {
            dummymethod4();
        }
        public static void dummymethod4()
        {
            throw new EntryPointNotFoundException();
        }
        #endregion
    }
}
