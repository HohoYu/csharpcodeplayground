﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CSharpPlayground.Modules;
using CSharpPlayground.Tests;

namespace CSharpPlayground.Tests
{
    public class TestCaseWindow
    {
        private enum Case
        {
            none, LoadTags, SelectedTags, ResetTags, TryChange, BoxWidth, PrintTagsInfo,
            PrintQuadrantTagsInfo, PrintViewInfo, TestArrange2
        }
        private enum Case2
        {
            none, LoadTags, SelectedTags, ResetTags, TryChange, BoxWidth, PrintTagsInfo,
            PrintQuadrantTagsInfo, PrintViewInfo, TestArrange2
        }
        public static void Test()
        {
            GeneralFunction.GetCase<Case>();
        }
        public static void Test2()
        {
            GeneralFunction.GetCase<Case2>();
        }
    }
}
