﻿//2018/06/20

using System.Windows.Forms;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;

namespace CSharpPlayground.Modules
{
    public class GeneralFunction
    {
        internal static GeneralFunction instance = new GeneralFunction();
        const double eps = 0.000000001;
        static int seed = 20180507;
        static Random r = new Random(seed);

        public static T GetCase<T>() where T : struct, IConvertible
        {
            var names = Enum.GetNames(typeof(T)).ToList();
            string name = typeof(T).Name;
            CaseListWindow w = new CaseListWindow(names, name);
            bool r = w.ShowDialog() ?? false;
            if (r)
            {
                var c = w.Case;
                return Enum.TryParse<T>(c.Text, out T Case) ? Case : default(T);
            }

            return default(T);
        }

        #region Maths

        public static double DtoR(double value)
        {
            return System.Math.PI * value / 180;
        }

        public static List<int> GenerateIntegerSequence(int Min, int Max)
        {
            var list = new List<int>();
            for (int i = Min; i < Max + 1; i++)
            {
                list.Add(i);
            }
            return list;
        }

        public static List<T> Randomize<T>(List<T> list)
        {
            var dummy = new List<T>(list);
            int n = list.Count;
            var NewList = new List<T>();
            for (int i = 0; i < list.Count; i++)
            {
                var item = dummy[r.Next(0, n - 1)];
                dummy.Remove(item);
                NewList.Add(item);
                n--;
            }

            return NewList;
        }

        public static bool EqualDouble(double a, double b)
        {
            return Math.Abs(a - b) <= eps;
        }
        #endregion

        #region DataType 

        public static List<object> ParseArray<T>(string a)
        {
            List<int> indexes = new List<int>();
            List<object> result = new List<object>();

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] == ' ')
                    indexes.Add(i);
                //Console.WriteLine(a[i]);
            }

            for (int i = 0; i < indexes.Count - 1; i++)
            {
                var start = indexes[i];
                var end = indexes[i + 1];
                var s = a.Substring(start + 1, end - start - 1);

                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.Int16:
                    case TypeCode.Int32:

                        result.Add(Convert.ToInt32(s));
                        break;

                    case TypeCode.Int64:
                        result.Add(Convert.ToInt64(s));
                        break;

                    case TypeCode.Double:
                        result.Add(Convert.ToDouble(s));
                        break;

                    case TypeCode.String:
                        result.Add(s);
                        break;

                    case TypeCode.Boolean:
                        result.Add(Convert.ToBoolean(s));
                        break;

                    default:
                        break;
                }
            }
            return result;
        }

        public static string SerializeArray(List<object> list)
        {
            string s = " ";
            list.ForEach(x => { s += $"{x} "; });

            return s;
        }

        #endregion

        #region I/O
        public static string OutRelative(string path)
        {
            var index = path.LastIndexOf("\\");
            path = path.Substring(index + 1);
            return path.Substring(0, path.Length - 4);
        }

        public static string ErrorMessage(string errorMsg, string position)
        {
            return string.Format("Error {0} \n Ocurr At {1}", errorMsg, position);
        }

        //public static void WriteErrorLog(Document doc, System.Exception ex)
        //{
        //    try
        //    {
        //        StackTrace st = new StackTrace(ex, true);
        //        StackFrame CallStack = st.GetFrame(0);
        //        var line = CallStack.GetFileLineNumber();

        //        string log = string.Format("\r\nFile Name: {0} \r\nTime: {1} \r\nCommand:{2} \r\nException:{3} \r\nStackTrace:{4} \r\nLine Number:{5} \r\nSource:{6}",
        //            doc.PathName, System.DateTime.Now, "", ex.Message, ex.StackTrace, line, ex.Source);
        //        string time = System.DateTime.Today.ToShortDateString();

        //        string[] Sp = new string[] { "/" };
        //        string[] result;

        //        result = time.Split(Sp, StringSplitOptions.RemoveEmptyEntries);
        //        time = result[2] + "-" + result[1] + "-" + result[0];

        //        string Directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        //        string path = string.Format(Directory + @"\Log\LD {0}.txt", string.Format("{0} Error", time));


        //    }
        //    catch (System.Exception Ex)
        //    {
        //        MessageBox.Show(ErrorMessage(Ex.Message, "OtherFunction - Write ErrorLog"));
        //    }

        //}

        internal void RevitReportCoder(Dictionary<List<string>, string> items)
        {
            string Report = "Autojoin error report: ";
            Report += "\r\n" + System.DateTime.Now.ToString();
            foreach (var item in items)
            {
                string record = "";
                foreach (var id in item.Key)
                {
                    record += string.Format(" \r\nElement id: {0}", id);
                }

                record += string.Format("\r\n\t Error: {0}", item.Value);
                Report += record;
            }
            instance.WriteText(Report);
        }

        private void WriteText(string text)
        {
            string path = SaveText();
            if (path == null)
                return;

            if (!System.IO.File.Exists(path))
                using (System.IO.StreamWriter file = System.IO.File.CreateText(path))
                {

                    file.Write(text);
                }

            else
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                {
                    file.Write(text);
                }
        }

        private string SaveText()
        {
            try
            {
                SaveFileDialog SF = new SaveFileDialog();
                SF.DefaultExt = "txt";
                SF.Filter = "Text files (*.txt)|*.txt";
                SF.InitialDirectory = "C:\\";
                SF.FileName = string.Format(".txt", "");

                DialogResult result = SF.ShowDialog();
                if (result == DialogResult.OK)
                    return SF.FileName;
                return null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public static void WriteJson(string text)
        {
            string path = SaveJson();
            if (path == null)
                return;

            if (!System.IO.File.Exists(path))
                using (System.IO.StreamWriter file = System.IO.File.CreateText(path))
                {

                    file.Write(text);
                }

            else
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                {
                    file.Write(text);
                }
        }

        public static void WriteJson(string text, string FileName, string Directory)
        {
            string path = SaveJson(FileName, Directory);
            if (path == null)
                return;

            if (!System.IO.File.Exists(path))
                using (System.IO.StreamWriter file = System.IO.File.CreateText(path))
                {

                    file.Write(text);
                }

            else
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                {
                    file.Write(text);
                }
        }

        public static void WriteJson(string text, string path)
        {
            if (path == null)
                return;

            if (!System.IO.File.Exists(path))
                using (System.IO.StreamWriter file = System.IO.File.CreateText(path))
                {

                    file.Write(text);
                }

            else
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                {
                    file.Write(text);
                }
        }

        private static string SaveJson()
        {
            try
            {
                SaveFileDialog SF = new SaveFileDialog();
                SF.DefaultExt = "json";
                SF.Filter = "JSON files (*.json)|*.json";
                SF.InitialDirectory = "C:\\";
                SF.FileName = string.Format(".json", "");

                DialogResult result = SF.ShowDialog();
                if (result == DialogResult.OK)
                    return SF.FileName;
                return null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private static string SaveJson(string FileName, string Directory)
        {
            try
            {
                SaveFileDialog SF = new SaveFileDialog();
                SF.DefaultExt = "json";
                SF.Filter = "JSON files (*.json)|*.json";
                SF.InitialDirectory = Directory; //"C:\\";
                SF.FileName = string.Format("{0}.json", FileName);

                DialogResult result = SF.ShowDialog();
                if (result == DialogResult.OK)
                    return SF.FileName;
                return null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        #endregion
    }

    public class Statistic
    {
        public static double sdv(List<double> list)
        {
            double dummy = 0;

            var mean = list.Average();
            dummy = Math.Sqrt(list.Sum(x => Math.Pow(x - mean, 2)) / (list.Count - 1));

            return dummy;


        }
    }

    public class ExpiryTimer
    {
        const string AppName = "";
        const int year = 2018;
        const int month = 10;
        const int day = 31;

        public static bool Expired()
        {
            if (System.DateTime.Today > new System.DateTime(year, month, day))
            {
                MessageBox.Show(string.Format($"The Beta ver. {AppName}-plugin was expired on {0}-{1}-{2}", day, month, year), "Info");
                return true;
            }
            else
                return false;
        }
    }
}

namespace CSharpPlayground.Modules.Graph
{
    class Graph
    {
        private int num_vertex;
        private List<List<int>> AdjList;
        private int[] color;
        private int[] predecessor;
        private int[] distance;
        private int[] discover;
        private int[] finish;

        public Graph() { num_vertex = 0; }
        public Graph(int N) { num_vertex = N; }

        public void AddEdgeList(int from, int to)
        {
            AdjList[from].Add(to);
        }

        public void BFS(int Start)
        {
            color = new int[num_vertex];
            predecessor = new int[num_vertex];
            distance = new int[num_vertex];

            for (int t = 0; t < num_vertex; t++)
            {
                color[t] = 0;
                predecessor[t] = -1;
                distance[t] = num_vertex + 1;
            }

            Queue<int> q = new Queue<int>();
            int i = Start;

            for (int j = 0; j < num_vertex; j++)
            {
                if (color[i] == 0)
                {
                    color[i] = 1;
                    distance[i] = 0;
                    predecessor[i] = -1;
                    q.Enqueue(i);
                    while (q.Count > 0)
                    {
                        int u = q.Peek();
                        foreach (var itr in AdjList[u])
                        {
                            if (color[itr] == 0)
                            {
                                color[itr] = 1;
                                distance[itr] = distance[u] + 1;
                                predecessor[itr] = u;
                                q.Enqueue(itr);
                            }
                        }
                        q.Dequeue();
                        color[u] = 2;
                    }
                }
                i = j;
            }
        }

        public void DFS(int Start)
        {
            color = new int[num_vertex];
            discover = new int[num_vertex];
            finish = new int[num_vertex];
            predecessor = new int[num_vertex];

            int time = 0;
            for (int t = 0; t < num_vertex; t++)
            {
                color[t] = 0;
                discover[t] = 0;
                finish[t] = 0;
                predecessor[t] = 0;
            }

            int i = Start;
            for (int j = 0; j < num_vertex; j++)
            {
                if (color[i] == 0)
                {
                    DFSVisit(i, time);
                }
                i = j;
            }
        }

        public void DFSVisit(int vertex, int time)
        {
            color[vertex] = 1;
            discover[vertex] = time++;
            foreach (var itr in AdjList[vertex])
            {
                if (color[itr] == 0)
                {
                    predecessor[itr] = vertex;
                    DFSVisit(itr, time);
                }
            }
            color[vertex] = 2;
            finish[vertex] = time++;
        }
    }

    public abstract class ForidaGraph<T>
    {
        public int num_vertex;
        public List<int>[] AdjList;
        public int[] color;
        public int[] predecessor;
        public int[] distance;
        public int[] discover;
        public int[] finish;

        public T[] objects;
        public List<Action> actions = new List<Action>();
        public delegate void Action(T obj);
        public delegate void Action2(int i, T obj);
        private Action act0 { get { return actions[0]; } }
        //private Action act1 { get { return actions[1]; } }
        //private Action act2 { get { return actions[2]; } }
        //private Action act3 { get { return actions[0]; } }
        //private Action act4 { get { return actions[1]; } }
        //private Action act5 { get { return actions[2]; } }
        public Action2 act1;

        public ForidaGraph() { num_vertex = 0; AdjList = new List<int>[0]; }
        public ForidaGraph(int N)
        {
            num_vertex = N;
            AdjList = new List<int>[num_vertex];
            for (int i = 0; i < num_vertex; i++)
            {
                AdjList[i] = new List<int>();
            }
            objects = new T[num_vertex];
        }

        public virtual void AddEdgeList(int from, int to)
        {
            AdjList[from].Add(to);
        }

        public virtual void BFS(int Start)
        {
            color = new int[num_vertex];
            predecessor = new int[num_vertex];
            distance = new int[num_vertex];

            //Initiation
            for (int t = 0; t < num_vertex; t++)
            {
                color[t] = 0;
                predecessor[t] = -1;
                distance[t] = num_vertex + 1;
            }

            Queue<int> q = new Queue<int>();
            int i = Start;

            for (int j = 0; j < num_vertex; j++)
            {
                if (color[i] == 0)
                {
                    color[i] = 1;
                    distance[i] = 0;
                    predecessor[i] = -1;
                    q.Enqueue(i);
                    while (q.Count > 0)
                    {
                        int u = q.Peek();
                        foreach (var itr in AdjList[u])
                        {
                            if (color[itr] == 0)
                            {
                                color[itr] = 1;
                                distance[itr] = distance[u] + 1;
                                predecessor[itr] = u;
                                q.Enqueue(itr);
                            }
                        }
                        q.Dequeue();
                        color[u] = 2;
                    }
                }
                i = j;
            }
        }

        public virtual void DFS(int Start)
        {
            color = new int[num_vertex];
            discover = new int[num_vertex];
            finish = new int[num_vertex];
            predecessor = new int[num_vertex];

            int time = 0;
            for (int t = 0; t < num_vertex; t++)
            {
                color[t] = 0;
                discover[t] = 0;
                finish[t] = 0;
                predecessor[t] = 0;
            }

            int i = Start;
            for (int j = 0; j < num_vertex; j++)
            {
                if (color[i] == 0)
                {
                    DFSVisit(i, time);
                }
                i = j;
            }
        }

        public virtual void DFSVisit(int vertex, int time)
        {
            color[vertex] = 1;
            discover[vertex] = time++;
            foreach (var itr in AdjList[vertex])
            {
                if (color[itr] == 0)
                {
                    predecessor[itr] = vertex;
                    DFSVisit(itr, time);
                }
            }
            act0(objects[vertex]);
            color[vertex] = 2;
            finish[vertex] = time++;
        }
    }

}

namespace CSharpPlayground.Modules.Geometry
{
    public class XYZ
    {
        public double X = 0;
        public double Y = 0;
        public double Z = 0;
        public static XYZ Origin = new XYZ(0, 0, 0);

        public XYZ(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }

    public enum Quadrant { none, I, II, III, IV }

    public class GeometryUtils
    {
        public static Quadrant GetQuadrant(XYZ pt, XYZ c)
        {
            var Q = Quadrant.none;
            if (pt.X > c.X && pt.Y > c.Y)
                Q = Quadrant.I;

            else if (pt.X < c.X && pt.Y > c.Y)
                Q = Quadrant.II;

            else if (pt.X < c.X && pt.Y < c.Y)
                Q = Quadrant.III;

            else
                Q = Quadrant.IV;

            return Q;
        }

        public static double GetYc(XYZ pt, double degree, Quadrant q)
        {
            double c = 0;
            double m = Math.Tan(GeneralFunction.DtoR(degree));

            switch (q)
            {
                case Quadrant.I:
                case Quadrant.III:
                    c = pt.Y - pt.X * m;
                    break;
                case Quadrant.II:
                case Quadrant.IV:
                    c = pt.Y + pt.X * m;
                    break;
            }

            return c;
        }
    }
}