﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Input;
using CSharpPlayground.Modules;



[assembly: CommandClass(typeof(ACADPlayground.TestCommand))]

namespace ACADPlayground
{
    public class TestCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            Main();
        }

        [CommandMethod("TestCommand")]
        public void Main()
        {
            Log4 log = new Log4("testappname", true);
            //for (int i = 0; i<1000; i++)
            //{
            //    log.Log("hello world");
            //}
            log.Log("hello world");
        }
    }
}